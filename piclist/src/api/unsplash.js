import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID du1dAp-LjGmqjspfnH_5L0gx-mLnMex4o1avocfKdTk'
  }
});
