import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';

const App = () => {
  return (
      <div className={'ui container comments'}>
        <div className='comment'>
          <a href="/" className='avatar'>
            <img alt='avatar' src={faker.image.avatar()} />
          </a>
        </div>
        <div className='comment'>
          <a href="/" className='author'>
           Sam
          </a>
        </div>
          <div className='metadate'>
              <span className='data'>Today at 6:00pm</span>
          </div>
      </div>
  );
};

ReactDOM.render(<App/>, document.querySelector('#root'));
